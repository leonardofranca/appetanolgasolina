package com.integrado.appetanolgasolina

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun calcular(view: View) {



        val precoGasolina = editGasolina.text.toString();
        val precoEtanol = editEtanol.text.toString();

        val validaCampos = validaCampo(precoEtanol, precoGasolina)

        if (validaCampos) {
            calculaMelhorPreco(precoEtanol, precoGasolina);
        } else {
            textResultado.setText("Preencha os dados primeiro")
        }
    }

    fun validaCampo(precoEtanol:String, precoGasolina:String) : Boolean {
        var camposValidados:Boolean = true;

        if (precoEtanol == null || precoEtanol.equals(""))  {
            camposValidados = false;
        } else if (precoGasolina == null || precoGasolina.equals("")) {
            camposValidados = false;
        }
        return camposValidados
    }

    fun calculaMelhorPreco (precoEtanol: String, precoGasolina: String) {
        val valorAlcool   = precoEtanol.toDouble();
        val valorGasolina = precoGasolina.toDouble();

        val resultadoPreco = valorAlcool/valorGasolina

        if (resultadoPreco >= 0.7) {
            textResultado.setText("Melhor usar Gasolina")
        } else {
            textResultado.setText("Melhor usar Alcool")
        }
    }
}
